<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Text extends CI_Controller {
// Index page (file upload view)
	public function index()
	{
		$this->load->view('admin/text_file/file_upload');
	}

// Upload and validate the txt file
	public function upload()
	{
// Create a folder to upload
		$folderName = date("Y-m-d");
		$target_dir = 'uploads/txt_files/'.$folderName."/";
		if(!is_dir($target_dir)) {
			mkdir($target_dir);
		}
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$moveOk = 0;
		$textFileType = $_FILES['fileToUpload']['type'];
// Check if file already exists
		if (file_exists($target_file)) {
			echo "Sorry, file already exists.<br/>";
			$uploadOk = 0;
		}
// Check file size
		if ($_FILES["fileToUpload"]["size"] > 500000) {
			echo "Sorry, your file is too large.<br/>";
			$uploadOk = 0;
		}
// Allow certain file formats
		if($textFileType != "text/plain") {
			echo "Sorry, only txt files are allowed.<br/>";
			$uploadOk = 0;
		}
// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			echo "Sorry, your file was not uploaded.<br/>";
// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.<br/>";
				$moveOk = 1;
			} else {
				echo "Sorry, there was an error uploading your file.<br/>";
				$moveOk = 0;
			}
		}
//redirect to 'import' controller
		if($uploadOk == 1 && $moveOk == 1){
			$this->import($target_file);
		}	
	}

	// Insert data in uploaded txt file to the database
	private function import($targetFile)
	{
		$text_array = array(); //to store lines in .txt file as an array
		$myfile = fopen($targetFile, "r") or die("Unable to open file!");
// Output one line until end-of-file
		while(!feof($myfile)) {
// Save one line in the file to a variable
			$space_line = fgets($myfile);
// Reduce multiple spaces from the string
			$line = preg_replace('/\s+/', ' ',$space_line);
// Add the line's content to an array
			if(strlen($line)>2){ //skip the empty lines
// Replace <space> with <,> (comma)
				$comma_line = str_replace(" ","@",$line);
// Seperate text from <space>, remove empty valurs and add to a local array
				$line_array = array_map('trim',array_filter(explode("@", $comma_line)));
// Add to the global array
				array_push($text_array,$line_array);
			}
		}
		fclose($myfile);

// Define the variable for each array index (assuming table has 15 fields)
		for ($i = 1; $i <= 14; $i++){
			${'field_'.$i}= "";
		}
// Create HTML table
		echo "<style>table, th, td {border: 1px solid black;}</style>";
		echo "<table>
				<tr>
				<th>Broker code</th>
				<th>Sale code</th>
				<th>Sale date</th>
				<th>Lot no.</th>
				<th>Manufacture code</th>
				<th>Garden mark</th>
				<th>Invoice no.</th>
				<th>Grade</th>
				<th>No. of bags</th>
				<th>Rate of one bag</th>
				<th>Total value of bags</th>
				<th></th>
				<th>Warehouse address</th>
				<th></th>
				</tr>";
// Extract the first array from the 2D array
		foreach ($text_array as $key) {
// Reset all variables
			for ($i = 1; $i <= 14; $i++){
				${'field_'.$i}= "";
			}

// Validation for first field
			if(isset($key[0]) && !is_numeric($key[0])){
				$field_1 = $key[0];
			}
// Validation for second field
			if(isset($key[1]) && is_numeric($key[1])){
		//check the number is greater than 5
				(int)$key[1]; //convert string to numeric value
				if($key[1] > 5){
					$field_2 = $key[1];
				}
				else{
					$field_9 = $key[1];	
				}	
			}
// Validation for third field
			if(isset($key[2]) && strpos($key[2], '/') !== false){
				$field_3 = $key[2];
			}
			else if(strpos($key[2], '.') !== false){
				$field_10 = $key[2];
			}
// Validation for forth field
			if(isset($key[3]) && is_numeric($key[3])){
				$field_4 = $key[3];
			}
// Validation for fifth field
			if(isset($key[4])){
				$field_5 = $key[4];
			}
// Validation for sixth field
			if(isset($key[5])){
				$field_6 = $key[5]; //field contain one name
			}
// Validation for seventh field
			if(isset($key[6]) && !preg_match('~[0-9]+~',$key[6])){ //field contain two names
				$field_6 .= " ".$key[6];
			}
			else if(isset($key[6]) && preg_match('~[0-9]+~',$key[6])){ //field contain one name
				$field_7 = $key[6];
			}
// Validation for eighth field
			if(isset($key[7]) && !preg_match('~[0-9]+~',$key[7]) && !preg_match('~[0-9]+~',$key[(7-1)])){ //field contain three names
				$field_6 .= " ".$key[7];
			}
			else if(isset($key[7]) && strpos($field_6, ' ') !== false){ //field contain two names
				$field_7 = $key[7];
			}
			else if(isset($key[7]) && strpos($field_6, ' ') == false){ //field contain one name
				$field_8 = $key[7];
			}
// Validation for nineth field
			if(isset($key[8]) && isset($key[9]) && strpos($field_6, ' ') == false){ //field contain one name
				$field_9 = $key[8].$key[9];//number of tea bags
			}
			else if(isset($key[8]) && substr_count($field_6, ' ') == 1){ //field contain two names
				$field_8 = $key[8];
			}
			else if(isset($key[8]) && substr_count($field_6, ' ') == 2){ //field contain three names
				$field_7 = $key[8];
			}
// Validation for tenth field
			if(isset($key[10]) && strpos($field_6, ' ') == false){ //field contain one name
				$field_10 = $key[10];
			}
			else if(isset($key[9]) && isset($key[10]) && substr_count($field_6, ' ') == 1){ //field contain two names
				$field_9 = $key[9].$key[10];//number of tea bags
			}
			else if(isset($key[9]) && substr_count($field_6, ' ') == 2){ //field contain three names
				$field_8 = $key[9];
			}
// Validation for eleventh field
			if(isset($key[11]) && strpos($field_6, ' ') == false){ //field contain one name
				$field_11 = $key[11];
			}
			else if(isset($key[11]) && substr_count($field_6, ' ') == 1){ //field contain two names
				$field_10 = $key[11];
			}
			else if(isset($key[10]) && isset($key[11]) && substr_count($field_6, ' ') == 2){ //field contain three names
				$field_9 = $key[10].$key[11]; //number of tea bags
			}
// Validation for twelveth field
			if(isset($key[12]) && strpos($field_6, ' ') == false){ //field contain one name
				$field_12 = $key[12];
			}
			else if(isset($key[12]) && substr_count($field_6, ' ') == 1){ //field contain two names
				$field_11 = $key[12];
			}
			else if(isset($key[12]) && substr_count($field_6, ' ') == 2){ //field contain three names
				$field_10 = $key[12];
			}
//=========================================Balance the filled fields========================================
// Validation for thirteenth field - PART 1
			if(isset($key[13]) && substr_count($field_6, ' ') == 1){ //field contain two names
				$field_12 = $key[13];
			}
			else if(isset($key[13]) && substr_count($field_6, ' ') == 2){ //field contain three names
				$field_11 = $key[13];
			}
// Validation for thirteenth field - PART 2(filling the same fields)
			if(isset($key[14]) && substr_count($field_6, ' ') == 2 && is_numeric($key[14])){ //field contain three names
				$field_12 = $key[14];
			}
//======================================End balance the filled===============================================

// // UPTO HERE ALL THE FIELDS HAS BEEN FILLED FROM FIELD_1 TO FIELD_12

// // Validate the last fields for field contain one name
			$x = 0;
			if(isset($key[13]) && substr_count($field_6, ' ') == false){
				$x = 13;
			}
	// Validate the last fields for field contain two name
			else if(isset($key[14]) && substr_count($field_6, ' ') == 1){
				$x = 14;
			}
	// Validate the last fields for field contain three name
			else if(isset($key[15]) && substr_count($field_6, ' ') == 2){
				$x = 15;
			}
			if($x >= 13){
				do{
					if(is_numeric($key[$x]) && !isset($key[($x+1)])){
						$field_14 = $key[$x];
					}
					else{
						$field_13 .= " ".$key[$x];
					}
					$x++;
				} while(isset($key[($x)]));
			}
// Print in the view as a table===============ADDITONAL
			echo "<tr>";
			for ($i = 1; $i <= 14; $i++){
				echo "<td>";
				echo ${'field_'.$i};
				echo "</td>";
			}
			echo "</tr>";
			

// Insert into database
			$data = array(
				'broker_code' => $field_1,
				'sale_code' => $field_2,
				'sale_date' => $field_3,
				'lot_no' => $field_4,
				'manufacture_code' => $field_5,
				'garden_mark' => $field_6,
				'invoice_no' => $field_7,
				'grade' => $field_8,
				'no_of_bags' => $field_9,
				'rate_of_one_bag' => $field_10,
				'total_value_of_bags' => $field_11,
				'field_12' => $field_12,
				'warehouse_address' => $field_13,
				'field_14' => $field_14,
			);

			$this->txt_file_model->insert($data,'tea_categories');
		}
		echo "</table>"; // End HTMLtable
	}
}