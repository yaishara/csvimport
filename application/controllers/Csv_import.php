<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Csv_import extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('csv_import_model');
		$this->load->library('csvimport');
	}

	function index()
	{
		$this->load->view('csv_import');
	}

	function load_data()
	{
		$result = $this->csv_import_model->select();
		$output = '
		 <h3 align="center">Imported User Details from CSV File</h3>
        <div class="table-responsive">
        	<table class="table table-bordered table-striped">
        		<tr>
        			<th>Sr. No</th>
        			<th>First Name</th>
        			<th>Last Name</th>
        			<th>Phone</th>
        			<th>Email Address</th>
        		</tr>
		';
		$count = 0;
		if($result->num_rows() > 0)
		{
			foreach($result->result() as $row)
			{
				$count = $count + 1;
				$output .= '
				<tr>
					<td>'.$count.'</td>
					<td>'.$row->first_name.'</td>
					<td>'.$row->last_name.'</td>
					<td>'.$row->phone.'</td>
					<td>'.$row->email.'</td>
				</tr>
				';
			}
		}
		else
		{
			$output .= '
			<tr>
	    		<td colspan="5" align="center">Data not Available</td>
	    	</tr>
			';
		}
		$output .= '</table></div>';
		echo $output;
	}

	function import()
	{
		// $file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"]);
		// foreach($file_data as $row):
		
		// 	$data[] = array(
		// 		'first_name'	=>	$row["First Name"],
  //       		'last_name'		=>	$row["Last Name"],
  //       		'phone'			=>	$row["Phone"],
  //       		'email'			=>	$row["Email"]
		// 	);
		// 	endforeach;
		// 	//$someJSON = json_encode($data);

		// 	foreach ($data as $key) {
		// 		$first_name=NULL;
		// 		$last_name=NULL;
		// 		$email=NULL;
		// 		if(!is_numeric($key['first_name'])){
		// 			$first_name=$key['first_name'];
		// 			// echo $first_name;
		// 			//echo json_encode($first_name);
		// 		}else{
		// 			//echo 'error';
		// 		}if(!is_numeric($key['last_name'])){
		// 			$last_name=$key['last_name'];
		// 			// echo $last_name;
		// 				//echo json_encode($last_name);
		// 		}else{
		// 			//echo "not string";
		// 		}
		// 		$a=$key['email'];
		// 		if (!filter_var($a, FILTER_VALIDATE_EMAIL)) {
  // 				$emailErr = "Invalid email format"; 
  // 				//echo $emailErr;
		// 		}else{
		// 			//echo $email;
		// 			$email=$key['email'];
		// 		}
		// 		$data1[]= array(
		// 			'first_name'=>$first_name,
		// 			'last_name'=>$last_name,
		// 			'email'=>$email
		// 	);

		// 	//echo json_encode($data1);
		// 	}

			
		// 	//print_r($someJSON);
		// 	$this->csv_import_model->insert($data1);
	
		// redirect('csv_import');

		//========================================================================================================
		if (isset($_FILES['csv_file'])) {
        $file   = $_FILES['csv_file'];
        // print_r($file);  just checking File properties

        // File Properties
        $file_name  = $file['name'];
        $file_tmp   = $file['tmp_name'];
        $file_size  = $file['size'];
        $file_error = $file['error'];

        // Working With File Extension
        $file_ext   = explode('.', $file_name);
        $file_fname = explode('.', $file_name);

        $file_fname = strtolower(current($file_fname));
        $file_ext   = strtolower(end($file_ext));
        $allowed    = array('txt','pdf','doc','ods','csv');


        if (in_array($file_ext,$allowed)) {
            //print_r($_FILES);


            if ($file_error === 0) {
                if ($file_size <= 5000000) {
                        $file_name_new     =  $file_fname . uniqid('',true) . '.' . $file_ext;
                        $file_name_new    =  uniqid('',true) . '.' . $file_ext;
                        $file_destination =  'uploads/' . $file_name_new;
                        // echo $file_destination;
                        if (move_uploaded_file($file_tmp, $file_destination)) {
                                echo "Cv uploaded";
                        }
                        else
                        {
                            echo "some error in uploading file".mysql_error();
                        }
//                        
                }
                else
                {
                    echo "size must bne less then 5MB";
                }
            }

        }
        else
        {
            echo "invalid file";
        }
}
		
	}
	
		
}
