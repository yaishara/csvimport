<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<style>
table, th, td {
  border: 1px solid black;
}
</style>
<head>
	<meta charset="utf-8">
	<title>txt file import</title>
</head>
<body>

	
<!-- Define an global array -->
<?php 
$text_array = array(); //to store lines in .txt file as an array
?>

	<!-- File read -->
	<?php
	$myfile = fopen(base_url('uploads/txt_files/')."JB45.txt", "r") or die("Unable to open file!");
// Output one line until end-of-file
	while(!feof($myfile)) {
	//save one line in the file to a variable
		$space_line = fgets($myfile);
	//reduce multiple spaces from the string
		$line = preg_replace('/\s+/', ' ',$space_line);
	//add the line's content to an array
	if(strlen($line)>2){ //skip the empty lines
		//replace <space> with <,> (comma)
		$comma_line = str_replace(" ","@",$line);
		//seperate text from <space>, remove empty valurs and add to a local array
		$line_array = array_map('trim',array_filter(explode("@", $comma_line)));
		//add to the global array
		array_push($text_array,$line_array);
	}
}
fclose($myfile);
?>
<!-- End file read -->

<!-- <pre>
<?php print_r($text_array); ?>
</pre> -->

<!-- define the variable for each array index (assuming table has 15 fields)-->
<?php 
	for ($i = 1; $i <= 15; $i++){
    	${'field_'.$i}= "";
	} 
 ?>

<!-- extract the first array from the 2D array-->
<?php
	foreach ($text_array as $key) {
		//reset all variables
		for ($i = 1; $i <= 15; $i++){
    		${'field_'.$i}= "";
		}

		//validation for first field
		if(isset($key[0]) && !is_numeric($key[0])){
			$field_1 = $key[0];
		}
		//validation for second field
		if(isset($key[1]) && is_numeric($key[1])){
			//check the number is greater than 5
			(int)$key[1]; //convert string to numeric value
			if($key[1] > 5){
				$field_2 = $key[1];
			}
			else{
				$field_9 = $key[1];	
			}	
		}
		//validation for third field
		if(isset($key[2]) && strpos($key[2], '/') !== false){
			$field_3 = $key[2];
		}
		else if(strpos($key[2], '.') !== false){
			$field_11 = $key[2];
		}
		//validation for forth field
		if(isset($key[3]) && is_numeric($key[3])){
			$field_4 = $key[3];
		}
		//validation for fifth field
		if(isset($key[4])){
			$field_5 = $key[4];
		}
		//validation for sixth field
		if(isset($key[5])){
			$field_6 = $key[5]; //field contain one name
		}
		//validation for seventh field
		if(isset($key[6]) && !preg_match('~[0-9]+~',$key[6])){ //field contain two names
			$field_6 .= " ".$key[6];
		}
		else if(isset($key[6]) && preg_match('~[0-9]+~',$key[6])){ //field contain one name
			$field_7 = $key[6];
		}
		//validation for eighth field
		if(isset($key[7]) && !preg_match('~[0-9]+~',$key[7]) && !preg_match('~[0-9]+~',$key[(7-1)])){ //field contain three names
			$field_6 .= " ".$key[7];
		}
		else if(isset($key[7]) && strpos($field_6, ' ') !== false){ //field contain two names
			$field_7 = $key[7];
		}
		else if(isset($key[7]) && strpos($field_6, ' ') == false){ //field contain one name
			$field_8 = $key[7];
		}
		//validation for nineth field
		if(isset($key[8]) && strpos($field_6, ' ') == false){ //field contain one name
			$field_9 = $key[8];
		}
		else if(isset($key[8]) && substr_count($field_6, ' ') == 1){ //field contain two names
			$field_8 = $key[8];
		}
		else if(isset($key[8]) && substr_count($field_6, ' ') == 2){ //field contain three names
			$field_7 = $key[8];
		}
		//validation for tenth field
		if(isset($key[9]) && strpos($field_6, ' ') == false){ //field contain one name
			$field_10 = $key[9];
		}
		else if(isset($key[9]) && substr_count($field_6, ' ') == 1){ //field contain two names
			$field_9 = $key[9];
		}
		else if(isset($key[9]) && substr_count($field_6, ' ') == 2){ //field contain three names
			$field_8 = $key[9];
		}
		//validation for eleventh field
		if(isset($key[10]) && strpos($field_6, ' ') == false){ //field contain one name
			$field_11 = $key[10];
		}
		else if(isset($key[10]) && substr_count($field_6, ' ') == 1){ //field contain two names
			$field_10 = $key[10];
		}
		else if(isset($key[10]) && substr_count($field_6, ' ') == 2){ //field contain three names
			$field_9 = $key[10];
		}
		//validation for twelveth field
		if(isset($key[11]) && strpos($field_6, ' ') == false){ //field contain one name
			$field_12 = $key[11];
		}
		else if(isset($key[11]) && substr_count($field_6, ' ') == 1){ //field contain two names
			$field_11 = $key[11];
		}
		else if(isset($key[11]) && substr_count($field_6, ' ') == 2){ //field contain three names
			$field_10 = $key[11];
		}
		//=========================================Balance the filled fields========================================
		//validation for thirteenth field - PART 1
		if(isset($key[12]) && strpos($field_6, ' ') == false){ //field contain one name
			$field_13 = $key[12];
		}
		else if(isset($key[12]) && substr_count($field_6, ' ') == 1){ //field contain two names
			$field_12 = $key[12];
		}
		else if(isset($key[12]) && substr_count($field_6, ' ') == 2){ //field contain three names
			$field_11 = $key[12];
		}
		//validation for thirteenth field - PART 2(filling the same fields)
		if(isset($key[13]) && substr_count($field_6, ' ') == 1){ //field contain two names
			$field_13 = $key[13];
		}
		else if(isset($key[13]) && substr_count($field_6, ' ') == 2){ //field contain three names
			$field_12 = $key[13];
		}
		//validation for thirteenth field - PART 3(filling the same fields)
		if(isset($key[14]) && substr_count($field_6, ' ') == 2 && is_numeric($key[14])){ //field contain three names
			$field_13 = $key[14];
		}
		//======================================End balance the filled===============================================

		//UPTO HERE ALL THE FIELDS HAS BEEN FILLED FROM FIELD_1 TO FIELD_13

		//validate the last fields for field contain one name
		$x = 0;
		if(isset($key[13]) && substr_count($field_6, ' ') == false){
			$x = 13;
		}
		//validate the last fields for field contain two name
		else if(isset($key[14]) && substr_count($field_6, ' ') == 1){
			$x = 14;
		}
		//validate the last fields for field contain three name
		else if(isset($key[15]) && substr_count($field_6, ' ') == 2){
			$x = 15;
		}
		if($x >= 13){
			
			do{
				if(is_numeric($key[$x]) && !isset($key[($x+1)])){
	    			$field_15 = $key[$x];
	    		}
	    		else{
	    			$field_14 .= " ".$key[$x];
	    		}
	    		$x++;
			} while(isset($key[($x)]));
		}

		//print in the view as a table
		echo "<table>";
		echo "<tr>";
			for ($i = 1; $i <= 15; $i++){
	    		echo "<th>";
	    		echo ${'field_'.$i};
	    		echo "</th>";
			}
		echo "</tr>";
		echo "</table>";

		//insert into database
		$data = array(
			'field_1' => $field_1,
			'field_2' => $field_2,
			'field_3' => $field_3,
			'field_4' => $field_4,
			'field_5' => $field_5,
			'field_6' => $field_6,
			'field_7' => $field_7,
			'field_8' => $field_8,
			'field_9' => $field_9,
			'field_10' => $field_10,
			'field_11' => $field_11,
			'field_12' => $field_12,
			'field_13' => $field_13,
			'field_14' => $field_14,
			'field_15' => $field_15,
		);

		$this->setting_model->insert($data,'category');
	}
?>

</body>
</html>