<?php

class Txt_file_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	function insert($Data,$table_name)
	{
		$this->db->insert($table_name, $Data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
		
	}

}